package org.example;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;


public class MainTest {
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testZeroArgumentsCase() {
        String[] arr = {};
        Main.main(arr);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testOneArgumentCase() {
        String[] arr = {"1"};
        Main.main(arr);
    }

    @Test
    public void testTenArgumentsCase() {
        String[] arr = {"1", "5", "6", "3", "4", "2", "7", "8", "9", "10"};
        String arr2 = "1 2 3 4 5 6 7 8 9 10";
        Main.main(arr);

        assertEquals(arr2, outputStreamCaptor.toString().trim());

    }

    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenArgumentsCase() {
        String[] arr = {"1", "5", "6", "3", "4", "2", "7", "8", "9", "10", "11"};
        Main.main(arr);
    }
}