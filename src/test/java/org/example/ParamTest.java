package org.example;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class ParamTest {
    private String[] input;
    private String output;

    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    public ParamTest(String[] input, String output) {
        this.input = input;
        this.output = output;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        String[] arr1 = {"1", "5", "6", "3", "4", "2", "7", "8", "9", "10"};
        String[] arr2 = {"1", "5", "6", "3", "4", "2", "7", "8", "9"};
        String[] arr3 = {"1", "5", "3", "4", "2",  "8", "10"};
        String[] arr4 = {"5", "7", "10"};
        return Arrays.asList(new Object[][]{
                {arr1, "1 2 3 4 5 6 7 8 9 10"},
                {arr2, "1 2 3 4 5 6 7 8 9"},
                {arr3, "1 2 3 4 5 8 10"},
                {arr4, "5 7 10"},

        });
    }

    @Test
    public void testMultipleParams() {
        Main.main(input);

        assertEquals(output, outputStreamCaptor.toString().trim());

    }
}