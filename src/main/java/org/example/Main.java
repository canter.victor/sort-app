package org.example;

import java.util.*;

public class Main {


    /**
     * @param args Accept as arguments at least two and up to ten Integers
     */
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        if (args.length < 2 || args.length > 10) {
            System.out.println("Incorrect number of arguments!");
            throw new IllegalArgumentException();
        }

        for (String s : args) {
            try {
                list.add(Integer.parseInt(s));
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException();
            }
        }

        Collections.sort(list);

        for (Integer i : list) {
            System.out.print(i + " ");
        }
    }



}
